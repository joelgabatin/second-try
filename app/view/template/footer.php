<footer>
	<div id="footer">
		<?php if($view != "contact"): ?>
		<div class="row">
			<div class="lower-contact">
				<div class="contact-title">
					<h3 class="red text-uppercase">Contact Us</h3>
					<p>LET US KNOW HOW WE CAN HELP</p>
				</div>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" disabled>Submit Form</button>
				</form>
			</div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="wrapper">
				<div class="left">
					<div class="icon email-icon text-left">	
						<span class="label">EMAIL</span>			
						<p class="email">
							<?php $this->info(["email","mailto"]); ?>
						</p>
					</div>
				</div>
					<div class="middle">
						<span class="logo-bottom">
							<img src="<?php echo URL ?>public/images/content/logo.jpg">
						</span>	
					</div>
					<div class="right">
						<div class="icon phone-icon text-left">	
							<span class="label">PHONE</span>		
							<p class="phone">
								<?php $this->info(["phone","tel"]); ?>
							</p>
						</div>
					</div>
			</div>
		</div>	
		<div class="bottom-nav">			
				<nav>
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>home">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
							<li <?php $this->helpers->isActiveMenu("aboutus"); ?>><a href="<?php echo URL ?>aboutus">ABOUT US</a></li>
							<li <?php $this->helpers->isActiveMenu("dumpster_rental"); ?>><a href="<?php echo URL ?>dumpster_rental">DUMPSTER RENTAL</a></li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
							<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews">REVIEWS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
					</ul>
				</nav>
		</div>		

		<div class="row">
			<div class="bot bot-footer">
				<div class="left">
					<p>FOLLOW US:</p>
					<img src="<?php echo URL ?>public/images/content/social-media.png">
				</div>
				<div class="middle">
					<span class="copy text-uppercase">
						© <?php echo date("Y"); ?> <?php $this->info("company_name"); ?>. All Rights Reserved 
						<?php if( $this->siteInfo['policy_link'] ): ?>
							<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>. 
						<?php endif ?>
					</span>
					<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>	
				</div>
				<div class="right">
					<p>WE ACCEPT</p>
					<img src="<?php echo URL ?>public/images/content/card.png">
				</div>
			</div>
		</div>
	</div>
</footer>

<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>

<script src="//ajax.googleapis.com/ajax/libs/webfont/1.6/webfont.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/app.js"></script>
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script src="<?php echo URL; ?>public/scripts/cookie-script.js"></script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<textarea id="g-recaptcha-response" class="destroy-on-load"></textarea>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>

	<script>
	var captchaCallBack = function() {
		$('.g-recaptcha').each(function(index, el) {
			var recaptcha = grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			$( '.destroy-on-load' ).remove();
		})
	};

	$('.consentBox').click(function () {
	    if ($(this).is(':checked')) {
	    	if($('.termsBox').length){
	    		if($('.termsBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
	    	}else{
	        	$('.ctcBtn').removeAttr('disabled');
	    	}
	    } else {
	        $('.ctcBtn').attr('disabled', true);
	    }
	});

	$('.termsBox').click(function () {
	    if ($(this).is(':checked')) {
			if($('.consentBox').is(':checked')){
	    		$('.ctcBtn').removeAttr('disabled');
	    	}
	    } else {
	        $('.ctcBtn').attr('disabled', true);
	    }
	});

	</script>
<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<script>
	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
	  showSlides(slideIndex += n);
	}


	function showSlides(n) {
	  var i;
	  var slides = document.getElementsByClassName("mySlides");
	  var dots = document.getElementsByClassName("dot");
	  if (n > slides.length) {slideIndex = 1}    
	  if (n < 1) {slideIndex = slides.length}
	  for (i = 0; i < slides.length; i++) {
	      slides[i].style.display = "none";  
	  }
	  // for (i = 0; i < dots.length; i++) {
	  //     dots[i].className = dots[i].className.replace(" active", "");
	  // }
	  slides[slideIndex-1].style.display = "block";  
	  // dots[slideIndex-1].className += " active";
	}
</script>

<script>
 WebFont.load({
    google: {
      families: ['Open Sans:400,700,800','Teko:300,400,500','Poppins:300,400,600,700,800','Century Gothic:300,400,700,800','Montserrat:300,400,700,800',]
    }
  });
</script>

<a class="cta" href="tel:<?php $this->info("phone") ;?>"><span style="display: block; width: 1px; height: 1px; overflow: hidden;">Call To Action Button</span></a>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>


