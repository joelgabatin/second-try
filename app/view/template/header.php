<?php //$this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php if ($view == "home"): ?>
    <link rel="canonical" href="<?php echo URL; ?>" />
    <?php endif; ?>
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="top">
				<div class="row">
					<div class="wrapper fr">
						<div class="fl">
							<p class="social">	
								<a href="#" target="_blank">f</a>
								<a href="#" target="_blank">g</a>
								<a href="#" target="_blank">l</a>
								<a href="#" target="_blank">i</a>
								<a href="#" target="_blank">t</a>
							</p>
						</div>
						<div class="fr">
							<div class="tel">
								<span class="label">CALL US TODAY:</span>
								<a href="#">614-843-9985</a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="navbar" class="bot">			
				<div class="row">
					<div class="logo fl">
						<img id="logo" src="<?php echo URL ?>public/images/content/logo.jpg?>">
					</div>
					<div class="fr">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>home">HOME</a></li>
									<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">SERVICES</a></li>
									<li <?php $this->helpers->isActiveMenu("aboutus"); ?>><a href="<?php echo URL ?>aboutus">ABOUT US</a></li>
									<li <?php $this->helpers->isActiveMenu("dumpster-rental"); ?>><a href="<?php echo URL ?>dumpster-rental">DUMPSTER RENTAL</a></li>
									<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">GALLERY</a></li>
									<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews">REVIEWS</a></li>
									<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">CONTACT US</a></li>
							</ul>
						</nav>
					</div>
					<div class="clearfix"></div>
				</div>	
			</div>	
		</div>
	</header>

	<?php if($view == "home"):?>
	<div id="banner">
		<div class="banner-img">
			<img src="<?php echo URL ?>public/images/content/banner.jpg">
		</div><!-- 
		<div class="banner-text">
			<div class="row">
				<p class="banner-title">Junk Removal & Home Improvement Specialist</p>
				<div class="btn-holder btn-short btn-green"><a>BOOK US NOW</a></div>
			</div>
		</div>
		<div class="lower-banner">
			<div class="row">
				<p class="lb-title fl">Top Rated Junk Removal & Home Improvement Company You Can Trust</p>
				<div class="btn-holder btn-long btn-green fr"><a href="#">GET A FREE ESTIMATE</a></div>
				<div class="clearfix"></div>
			</div>
		</div> -->
	</div>
	<?php endif; ?>