<div id="content">
		<div class="line">
			<img src="<?php echo URL ?>public/images/content/arrow.png">
		</div>	
		<div class="welcome">			
			<div class="row">
				<div class="fl">	
					<h1 class="red"><span class="header font2">WELCOME TO</span> JUNK FELLAS LLC</h1>		
					<span class="sub-heading font2">Junk Removal & Home Improvement Specialist</span>
					<p>We are a family based company founded in Columbus, Ohio. We provide Junk removal / Home Improvement, Trash & Garbage Hauling Services in all of Columbus Ohio and surrounding areas. We are here to help you get rid of your unwanted stuff, trash, junk, material, waste, etc.  Junk Fellas previously known as First Choice Services LLC has provided quality services to clients by providing them with the professional care they deserve. Get in touch today to learn more about our Home Improvement and Junk Removal Services.</p>
					<div class="btn-holder btn-long btn-red btn-right"><a href="#" class="align-left">GET A FREE ESTIMATE</a></div>
				</div>
				<div class="fr">
					<img src="<?php echo URL ?>public/images/content/welcome-img-01.jpg">
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="line">
			<img src="<?php echo URL ?>public/images/content/arrow.png">
		</div>
		<div class="services">
			<div class="row">
				<div class="first-paragraph">
					<h2 class="red"> JUNK <span class="green">REMOVAL</span> SERVICES</h2>
					<p class="text-bold">You can count on Junk Fellas LLC to not only meet, but exceed all of your needs and requests. Learn more about the services we provide below.</p>
				</div>
				<div class="services-wrappers">		
					<div class="wrapper service1">
						<img src="<?php echo URL ?>public/images/content/services-img-01.png">
						<span class="red">
							RESIDENTIAL CLEAN OUTS
						</span>
						<ul>
							<li>Attic & Basement Clean outs</li>
							<li>Eviction Clean Outs</li>
							<li>Foreclosure Clean Outs</li>
							<li>Full Estate Clean Out Services</li>
							<li>Garage Clean Out Services</li>
							<li>Yard Waste Removal </li>
						</ul>
					</div>
					<div class="wrapper service2">
						<img src="<?php echo URL ?>public/images/content/services-img-02.png">
						<span class="red">RESIDENTIAL JUNK REMOVAL</span>
						<ul>
							<li>Appliance Removal & Recycling</li>
							<li>Satellite Removal & Recycling</li>
							<li>Carpet Removal & Disposal</li>
							<li>Furniture Removal & Disposal</li>
							<li>Trampoline Removal & Disposal</li>
							<li>Hot Tub Removal & Disposal</li>
							<li>Mattress Removal & Disposal</li>
							<li>Play set Removal & Disposal</li>
							<li>TV Removal & Disposal</li>
						</ul>
					</div>
					<div class="wrapper service3">
						<img src="<?php echo URL ?>public/images/content/services-img-03.png">
						<span class="red">COMMERCIAL JUNK REMOVAL</span>
						<ul>
							<li>Apartment Clean outs</li>
							<li>Tenant Clean Out </li>
							<li>Eviction Clean Out </li>
							<li>Storage Unit Clean Out Services</li>
							<li>Construction Site Clean up</li>
							<li>Electronics Removal & Disposal</li>
							<li>Hotel Furniture Removal & Recycling</li>
							<li>Office Furniture Removal & Recycling</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="improve text-center">
			<div class="row">
				<h3 class="red">HOME <span class="green">IMPROVEMENT</span> SERVICES</h3>
				<p>You can count on Junk Fellas LLC to not only meet, but exceed all your needs and requests. Learn more about the services we provide below, and let us know if you’d like to learn more about a specific offering.</p>
				<div class="btn-holder btn-long btn-red">
					<a href="#" class="align-left">LEARN MORE</a>
				</div>
			</div>
		</div>
		<div class="line">
			<img src="<?php echo URL ?>public/images/content/arrow.png">
		</div>
		<div class="prices">
			<div class="row">
				<h3 class="red">FLAT RATE, <span class="green">UPFRONT PRICES</span></h3>
				<div class="upper-paragraph">
					<p>Junk Fellas LLC provides professional junk, debris and trash removal services at a fair affordable rate for the Columbus Ohio area. Our website provides you with the opportunity to schedule a hassle free appointment which gets us on your project faster.</p>
					<p>Check out the load sizes and estimate prices below. If you are unsure of your load size just send us a picture of what you are trying to remove and we will help with the rest.</p>
					<p>Once we arrive to your property to complete the work just watch the junk disappear. We will take the stress off your shoulders. Give us a call or text at 614-843-9985</p>
				</div>
				<div class="wrapper">
					<div class="one all">
					<p class="prices-title">INDIVIDUAL PIECE REMOVAL</p>
					<ul>
						<li>Prices starting at $70*</li>
					</ul>
					</div>
					<div class="two all">
						<p class="prices-title">1/2 TRAILER LOAD</p>
						<ul>
							<li>$ 125 including labor and dump fees</li>
							<li>3.5 Ft Long x 7 Ft Wide x 5 Ft Tall </li>
							<li>4 Cubic Yard </li>
						</ul>
					</div>
					<div class="three all">
						<p class="prices-title ">1/2 TRAILER LOAD</p>
						<ul>
							<li>$ 245 including labor and dump fees </li>
							<li>7 Ft Long x 7 Ft Wide x 5 Ft Tall </li>
							<li>9 Cubic Yard </li>
						</ul>
					</div>
					<div class="four all">
						<p class="prices-title">FULL TRAILER LOAD</p>
						<ul>
							<li>$ 355 including labor and dump fees  </li>
							<li>14 Ft Long x 7 Ft Wide x 5 Ft Tall </li>
							<li>18 Cubic Yard </li>
						</ul>
					</div>
				</div>
				<div class="text-bold lower-paragraph">
					<p>Prices will vary  with  construction  material, concrete, tile, rocks & other heavy materials / also loading distance</p>
					<p>No Matter the job, Junk Fellas LLC has you covered. Contact us now.</p>
				</div>				
				<div class="btn-holder btn-short btn-red"><a href="#" class="align-left">BOOK US NOW</a></div>
			</div>
		</div>
		<div class="line">
			<img src="<?php echo URL ?>public/images/content/arrow.png">
		</div>
		<div class="about">
			<div class="row">
				<div class="fl text-center">
					<div class="about-paragraph text-left">
						<h3 class="red">ALL ABOUT JUNK FELLAS</h3>
						<p class="first-paragraph">Ishmeal and Tracey started Junk Fellas after Ishmeal spent some time with the State Highway Patrol. Having small children, Ishmeal and Tracey wanted to create a business that would keep Ishmeal closer to home and still be a help to their community. Ishmeal already had previous experience owning a commercial cleaning business and doing minor residential cleanings for real estate agents so starting the Junk Removal business was an easy transition.</p>
						<p>Since opening the business, they have been committed to providing service of the highest quality, paying particular attention to working efficiently while keeping the lines of communication with their clients clear and concise. They service Columbus Ohio and many of the surrounding cities.</p>
						<p>Their mission at Junk Fellas is simple: to provide high-quality services in a timely manner. Their team caters to each project’s specific needs to ensure excellence. We hope you’ll find what you’re looking for. If you need help getting rid of anything we can help you today.</p>
					</div>
				</div>
				<div class="fr">
						<div class="back">
							<img src="<?php echo URL ?>public/images/content/about-img-01.jpg">
						</div>						
						<div class="front">
							<img src="<?php echo URL ?>public/images/content/about-img-02.jpg">
						</div>
				</div>
				<div class="clearfix"></div>
			</div>	
		</div><!-- 
		<div class="line">
			<img src="<?php echo URL ?>public/images/content/arrow.png">
			<h3></h3>
		</div> -->
		<div class="review text-center">
			<div class="row">		
				<div class="arrow">
					<img src="<?php echo URL ?>public/images/content/arrow.png">		
				</div>	
				<div class="review-content">
				<div class="review-title">
					<h2 class="red">CLIENT <span class="green">REVIEWS</span></h2>
				</div>
					<div class="slideshow-container">						
						<div class="mySlides">
							<p class="review-paragraph">This was a great company! They had a dumpster/trailer available the day I asked for one. I felt they were reasonably priced and Ishmeal was an awesome man to do business with. He was very polite and respectful, customer satisfaction driven, and overall a nice person. I HIGHLY recommend this company for house clean outs or junk hauling. Thank you!!</p>
							<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
							<p class="rater text-bold text-center">-Patrick Elston</p>
						</div>
						<div class="mySlides">
							<p class="review-paragraph">But man is not made for defeat. A man can be destroyed but not defeated.</p>
							<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
							<p class="rater text-bold text-center">-Ernest Hemingway</p>
						</div>
						<div class="mySlides">
							<p class="review-paragraph">I have not failed. I've just found 10,000 ways that won't work.</p>
							<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
							<p class="rater text-bold text-center">-Thomas A. Edison</p>
						</div>					
						<a class="prev" onclick="plusSlides(-1)">❮</a>
						<a class="next" onclick="plusSlides(1)">❯</a>
					</div>
				</div>
			</div>
		</div>

		<div class="gallery-section">
			<ul>
				<li class="wrapper"><img src="<?php echo URL ?>public/images/content/review-img-01.jpg"></li>
				<li class="wrapper"><img src="<?php echo URL ?>public/images/content/review-img-02.jpg"></li>
				<li class="wrapper"><img src="<?php echo URL ?>public/images/content/review-img-03.jpg"></li>
				<li class="wrapper"><img src="<?php echo URL ?>public/images/content/review-img-04.jpg"></li>
			</ul>
		</div>
	
</div>
