/**
 * File app.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
	'use strict';
$( window ).scroll(function() {

	if( $(window).scrollTop() > 100){
		$(".logo").addClass("logo-small");
		$("#navbar").addClass("navbarcss");
	}else{
		$(".logo").removeClass("logo-small");
		$("#navbar").removeClass("navbarcss");
	}
});
	

} )( jQuery );